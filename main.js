// https://stackoverflow.com/questions/51130743/how-to-open-links-in-default-browser-using-electron
const { app, BrowserWindow, Menu, MenuItem } = require('electron')

app.once('ready', () => {

  const handleRedirect = (e, url) => {
    if (url !== e.sender.getURL()) {
      e.preventDefault()
      shell.openExternal(url)
    }
  }
  const win = new BrowserWindow()
  // Instead bare webContents:
  win.webContents.on('will-navigate', handleRedirect)
  win.loadURL('https://drive.google.com/file/d/19mwVKjY1JnXcj6ZE9GPLluDcycfNTfN5/view?usp=sharing')
  let timerToRefresh = setInterval(function() {
    win.reload()
  }, 4 * 60 * 1000); // 60 * 1000 milsec

  const menu = new Menu()
  menu.append(new MenuItem(
    {
      label: 'Registos de entrada',
          click: () => { 
            clearInterval(timerToRefresh);
            timerToRefresh = setInterval(function() {
              win.reload()
            }, 4 * 60 * 1000); // 60 * 1000 milsec
            win.loadURL('https://drive.google.com/file/d/19mwVKjY1JnXcj6ZE9GPLluDcycfNTfN5/view?usp=sharing'); 
        }
    }))
  menu.append(new MenuItem(
    {
      label: 'Registos diários de desvios',
          click: () => { 
            clearInterval(timerToRefresh);
            timerToRefresh = setInterval(function() {
              win.reload()
            }, 4 * 60 * 1000); // 60 * 1000 milsec
            win.loadURL('https://drive.google.com/file/d/1X9lOzWM5KGGXAtYMO16sXPniE0J65XyB/view?usp=sharing'); 
          }
    }))
  Menu.setApplicationMenu(menu);

})